//
//  MEAlertView.swift
//
//  Created by Mitsuharu Emoto on 2014/09/17.
//  Copyright (c) 2014年 Mitsuharu Emoto. All rights reserved.
//

import UIKit

typealias ShownCompletion = (() -> Void)
typealias ClickedCompletion = ((buttonIndex:Int)->Void)

class MEAlertView: NSObject, UIAlertViewDelegate {
    
    internal var tag:Int!;
    internal var buttonTitles:[String]!;
    private var alertView:AnyObject!;
    private var clickedCompletion:ClickedCompletion? = nil;
    
    override init() {
        super.init();
    }
    
    init(title aTitle: String,
        message aMessage: String,
        completion aCompletion:ClickedCompletion? = nil,
        cancelButtonTitle aCancelButtonTitle: String?,
        otherButtonTitles firstButtonTitle:String?, _  aOtherButtonTitles: String...)
    {
        super.init();
        dispatch_async(dispatch_get_main_queue(), {
            
            self.buttonTitles = [String]();
            
            if(MEAlertView.hasAlertController()){
 
                var alert:UIAlertController = UIAlertController(title: aTitle,
                    message: aMessage,
                    preferredStyle: UIAlertControllerStyle.Alert);
                self.alertView = alert;
                
                if let cancel = aCancelButtonTitle{
                    self.buttonTitles.append(cancel);
                }
                if let first = firstButtonTitle{
                    self.buttonTitles.append(first);
                }
                if aOtherButtonTitles.isEmpty != false{
                    for title:String in aOtherButtonTitles{
                        self.buttonTitles.append(title);
                    }
                }
                
                for index in 0..<(self.buttonTitles.count){
                    alert.addAction(UIAlertAction(title: self.buttonTitles[index],
                        style: UIAlertActionStyle.Default,
                        handler: { (alertAction) -> Void in
                            if let comp = aCompletion{
                                comp(buttonIndex: index);
                            }
                    }));
                }
              
            }else{
                var alert:UIAlertView = UIAlertView();
                self.alertView = alert;
                
                alert.delegate = self;
                alert.title = aTitle;
                alert.message = aMessage;
                if let cancel = aCancelButtonTitle{
                    alert.addButtonWithTitle(cancel);
                    self.buttonTitles.append(cancel);
                }
                if let first = firstButtonTitle{
                    alert.addButtonWithTitle(first);
                    self.buttonTitles.append(first);
                }
                
                for title:String in aOtherButtonTitles{
                    alert.addButtonWithTitle(title);
                    self.buttonTitles.append(title);
                }

                if let comp = aCompletion{
                    self.clickedCompletion = comp;
                }
            }
        });
    }
    
    deinit{
        self.buttonTitles = nil;
        self.clickedCompletion = nil;
        self.alertView = nil;
    }
    
    class func hasAlertController() -> Bool {
        var alertClass: AnyClass! = NSClassFromString("UIAlertController");
        var result:Bool = ((alertClass) != nil)
        return result
    }
    
    func show(viewController:UIViewController? = nil,
        completion aCompletion:ShownCompletion? = nil)
    {
        dispatch_async(dispatch_get_main_queue(), {
            if(MEAlertView.hasAlertController()){
                if let vc = viewController{
                    var alert:UIAlertController = self.alertView as UIAlertController;
                    vc.presentViewController(alert,
                        animated: true,
                        completion: { () -> Void in
                            if let comp = aCompletion{
                                comp();
                            }
                    });
                }
            }else{
                var alert:UIAlertView = self.alertView as UIAlertView;
                alert.show();
            }
        });
    }
    
    // MARK: - UIAlertViewDelegate
    
    func alertView(alertView: UIAlertView,
        clickedButtonAtIndex buttonIndex: Int)
    {
        if let comp = self.clickedCompletion{
            comp(buttonIndex: buttonIndex);
        }
    }
    
}
